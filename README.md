# RCC - the remote C compiler

## Source code
The source of RCC is [hosted on GitLab.com](https://gitlab.com/JohnMoon/rcc).
Feature requests and bug reports are welcome!

## Build projects with flexibility and speed
RCC is a tool that allows you to harness the power of a development machine
to compile large programs while abstracting away all the difficulty.

RCC operates with a simple client-server relationship. The RCC client can be
invoked exactly like gcc, but then the source and compiler flags are forwarded
on the the RCC server - typically a more powerful machine with more processing
capability.

The RCC configuration file defines what architecture you're compiling for,
which RCC server(s) you can connect to, and how you'd like to pass your code
to the RCC server.

The RCC server can be configured to compile code however you'd like based on
the architecture and flags passed from the client. If multiple machines are
available, distributed compiling can be performed by the server.

## Building
Once you download the source code, you can run `make` to build the client
executable. To build the server executable, run `make rccd`. To build both,
run `make all`. To install rcc/rccd, run `make install` with administrator
privileges.

## Dependencies
RCC currently depends on only one non-standard library - [libssh](https://www.libssh.org/).
Most Linux distributions have libssh installed by default.
