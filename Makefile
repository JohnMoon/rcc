rcc: $(CURDIR)/rcc/*
	$(MAKE) -C $(CURDIR)/rcc/src

rccd: $(CURDIR)/rccd/*
	$(MAKE) -C $(CURDIR)/rccd/src

all:
	$(MAKE) rcc && $(MAKE) rccd

install:
	cp $(CURDIR)/rcc/bin/rcc /usr/local/bin/rcc
	cp $(CURDIR)/rccd/bin/rccd /usr/local/bin/rccd

clean:
	-@rm $(CURDIR)/rcc/bin/* 2> /dev/null || true
	-@rm $(CURDIR)/rccd/bin/* 2> /dev/null || true
