###############################################################################
#
# File: rccd-config-gen.sh
#
# Purpose: Generates a simple configuration file for RCCD in the user's home
#          directory.
#
###############################################################################

#!/bin/sh

mkdir -p "${HOME}/.rcc/"
conf="${HOME}/.rcc/rccd_config"

printf "PORT=22\n" > "$conf"
printf "HOSTCC=gcc\n" >> "$conf"
