#include "rccd.h"

void print_help() {
	fprintf(stdout, "Usage: rccd [OPTIONS]\n");
	fprintf(stdout, "Options:\n");
	fprintf(stdout, "  -c <config path>\tstart the daemon with <config path> configuration\n");
	fprintf(stdout, "  --help\tPrint this help\n\n");
	fprintf(stdout, "For more documentation, please see:\n");
	fprintf(stdout, "<https://gitlab.com/JohnMoon/rcc>\n");
}

int main(int argc, char **argv) {
	char command[64];
	char *path;
	if (argc == 2) {
		path = malloc(strlen(argv[1]));
		strcpy(path, argv[1]);
	}
	// Generate system command based on input data
	sprintf(command, "gcc -o ~/.rccd_build/a.out %s", path);
	system(command);
}
