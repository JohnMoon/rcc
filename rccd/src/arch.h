/* Architecture structures for future development */
#include <stdbool.h>

typedef struct {
	bool is64;
} Arch;

typedef struct {
	Arch alpha;
	Arch arc;
	Arch arm;
	Arch arm64;
	Arch avr32;
	Arch blackfin;
	Arch c6x;
	Arch cris;
	Arch frv;
	Arch h83000;
	Arch hexagon;
	Arch ia64;
	Arch m32r;
	Arch m68k;
	Arch metatag;
	Arch microblaze;
	Arch mips;
	Arch mn10300;
	Arch nios2;
	Arch openrisc;
	Arch parisc;
	Arch powerpc;
	Arch s390;
	Arch score;
	Arch sh;
	Arch sparc;
	Arch tile;
	Arch um;
	Arch unicore32;
	Arch x86;
	Arch x86_64;
	Arch xtensa;
} Arches;
