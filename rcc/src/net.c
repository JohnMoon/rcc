#include "net.h"

ssh_session establish_connection(char *host, char *ssh_username, uint16_t port)
{
	ssh_session rcc_ssh_session = ssh_new();
	int	verbosity = 0; // TODO get RCC global verbosity
	if (rcc_ssh_session == NULL)
		exit (-1);

	// Set SSH options before connection
	ssh_options_set(rcc_ssh_session, SSH_OPTIONS_HOST, host);
	ssh_options_set(rcc_ssh_session, SSH_OPTIONS_USER, ssh_username);
	ssh_options_set(rcc_ssh_session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
	ssh_options_set(rcc_ssh_session, SSH_OPTIONS_PORT, &port);

	// Establish connection
	int rc = ssh_connect(rcc_ssh_session);
	if (rc != SSH_OK) {
		fprintf(stderr, "Error connecting to host %s:%d: %s\n",
				host, port,
				ssh_get_error(rcc_ssh_session));
		exit(-1);
	}

	int state = ssh_is_server_known(rcc_ssh_session);
	char buf[10];

	// Check for server state
	switch (state) {
		case SSH_SERVER_KNOWN_OK:
			break; /* ok */
		case SSH_SERVER_KNOWN_CHANGED:
			fprintf(stderr, "Host key for server changed: it is now:\n");
			fprintf(stderr, "For security reasons, connection will be stopped\n");
			exit(-1);
		case SSH_SERVER_FOUND_OTHER:
			fprintf(stderr, "The host key for this server was not found but an other"
				"type of key exists.\n");
			fprintf(stderr, "An attacker might change the default server key to"
			"confuse your client into thinking the key does not exist\n");
			exit(-1);
		case SSH_SERVER_FILE_NOT_FOUND:
			fprintf(stderr, "Could not find known host file.\n");
			fprintf(stderr, "If you accept the host key here, the file will be"
			"automatically created.\n");

		// TODO add behaviour for when server is unknown
		case SSH_SERVER_NOT_KNOWN:
			fprintf(stderr,"The server is unknown. Do you trust the host key?\n");
			if (fgets(buf, sizeof(buf), stdin) == NULL) {
				exit(-1);
			}
			if (strncasecmp(buf, "yes", 3) != 0) {
				exit(-1);
			}
			if (ssh_write_knownhost(rcc_ssh_session) < 0) {
				fprintf(stderr, "Error\n");
				exit(-1);
			}
			break;

		case SSH_SERVER_ERROR:
			fprintf(stderr, "Error %s", ssh_get_error(rcc_ssh_session));
			exit(-1);
	}

	// Try to authenticate with publickey
	rc = ssh_userauth_publickey_auto(rcc_ssh_session, NULL, NULL);
	if (rc != SSH_AUTH_SUCCESS) {
		fprintf(stderr, "Error authenticating with public key: %s\n",
		ssh_get_error(rcc_ssh_session));
		fprintf(stderr, "Attempting password authentication...\n");
	} else {
		if (verbosity)
			fprintf(stderr, "Authenticated using publickey...\n");
		return rcc_ssh_session;
	}

	// If publickey fails, try authenticating with password
	char *password = getpass("Password: ");
	rc = ssh_userauth_password(rcc_ssh_session, NULL, password);
	if (rc != SSH_AUTH_SUCCESS) {
		fprintf(stderr, "Error authenticating with password: %s\n",
		ssh_get_error(rcc_ssh_session));
		ssh_disconnect(rcc_ssh_session);
		ssh_free(rcc_ssh_session);
		exit(-1);
	} else {
		if (verbosity)
			fprintf(stderr, "Authenticated using password...\n");
		return rcc_ssh_session;
	}
}

void disconnect(ssh_session session) {
	ssh_disconnect(session);
	ssh_free(session);
}

int send_file(ssh_session session, char *path) {
	ssh_scp scp;
	int rc;

	// Create new SCP session
	scp = ssh_scp_new(session, SSH_SCP_WRITE | SSH_SCP_RECURSIVE, ".");
	if (scp == NULL) {
		fprintf(stderr, "error - could not allocate scp session: %s\n", ssh_get_error(session));
		return SSH_ERROR;
	}

	// Initialize the SCP session
	rc = ssh_scp_init(scp);
	if (rc != SSH_OK) {
		fprintf(stderr, "error - could not initialize scp session: %s\n", ssh_get_error(session));
		ssh_scp_free(scp);
		return rc;
	}

	// Specify which directory to push to on SSH host
	rc = ssh_scp_push_directory(scp, ".rccd_build", S_IRWXU);
	if (rc != SSH_OK) {
		fprintf(stderr, "error - could not create remote directory: %s\n", ssh_get_error(session));
		return rc;
	}

	struct stat st;
	stat(path, &st);

	// Specify which filespace to push to on SSH host
	rc = ssh_scp_push_file(scp, path, st.st_size, S_IRUSR | S_IWUSR);
	if (rc != SSH_OK) {
		fprintf(stderr, "error - could not open remote file: %s\n", ssh_get_error(session));
		return rc;
	}

	// Fill buffer with file data
	char *buffer = malloc((sizeof(char) * st.st_size) + 1);
	FILE *fd = fopen(path, "r");
	if (fd != NULL) {
		size_t newLen = fread(buffer, sizeof(char), st.st_size, fd);
		if (newLen == 0)
			fprintf(stderr, "error - could not read file");
		else
			buffer[newLen + 1] = '\0';
	}
	fclose(fd);

	// Write file to remote location
	rc = ssh_scp_write(scp, buffer, st.st_size);
	if (rc != SSH_OK) {
		fprintf(stderr, "error - could not write remote file: %s\n", ssh_get_error(session));
		return rc;
	}

	free(buffer);
	ssh_scp_close(scp);
	ssh_scp_free(scp);
	return SSH_OK;
}

int get_file(ssh_session session, char *path) {
	ssh_scp scp;
	int rc;

	// Create new SCP session linked to remote path
	scp = ssh_scp_new(session, SSH_SCP_READ, path);
	if (scp == NULL) {
		fprintf(stderr, "error - could not allocate scp session: %s\n", ssh_get_error(session));
		return SSH_ERROR;
	}

	// Initialize SCP session
	rc = ssh_scp_init(scp);
	if (rc != SSH_OK) {
		fprintf(stderr, "error - could not initialize scp session: %s\n", ssh_get_error(session));
		ssh_scp_free(scp);
		return rc;
	}

	// Specify that we're reading information about file
	rc = ssh_scp_pull_request(scp);
	if (rc != SSH_SCP_REQUEST_NEWFILE) {
		fprintf(stderr, "error - could not receive information about file: %s\n",
		ssh_get_error(session));
		return SSH_ERROR;
	}

	// Get file characteristics
	int mode = ssh_scp_request_get_permissions(scp);
	char *filename = strdup(ssh_scp_request_get_filename(scp));
	size_t size = ssh_scp_request_get_size(scp);
	printf("Receiving file %s, size %d, permisssions 0%o\n", filename, size, mode);

	// Read file into buffer
	uint8_t *buffer = malloc(sizeof(uint8_t) * size);
	ssh_scp_accept_request(scp);
	rc = ssh_scp_read(scp, buffer, size);
	if (rc == SSH_ERROR) {
		fprintf(stderr, "error - could not recieve remote file: %s\n", ssh_get_error(session));
		free(buffer);
		return rc;
	}

	// Write file into local filesystem
	FILE *fd = fopen(filename, "w");
	if (fd != NULL)
		fwrite(buffer, sizeof(uint8_t), size, fd);
	chmod(filename, mode);
	fclose(fd);
	free(buffer);

	ssh_scp_close(scp);
	ssh_scp_free(scp);
	return SSH_OK;
}

int send_command(ssh_session session, char *command)
{
	// Create new SSH channel
	ssh_channel channel;
	int rc;
	char buffer[256];
	unsigned int nbytes;
	channel = ssh_channel_new(session);
	if (channel == NULL)
		return SSH_ERROR;
	rc = ssh_channel_open_session(channel);
	if (rc != SSH_OK) {
		ssh_channel_free(channel);
		return rc;
	}

	// Request command execution
	rc = ssh_channel_request_exec(channel, command);
	if (rc != SSH_OK) {
		ssh_channel_close(channel);
		ssh_channel_free(channel);
		return rc;
	}

	// Get return from command
	nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
	while (nbytes > 0) {
		if (write(1, buffer, nbytes) != nbytes) {
			ssh_channel_close(channel);
			ssh_channel_free(channel);
			return SSH_ERROR;
		}
		nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
	}

	// Close channel
	if (nbytes < 0) {
		ssh_channel_close(channel);
		ssh_channel_free(channel);
		return SSH_ERROR;
	}
	ssh_channel_send_eof(channel);
	ssh_channel_close(channel);
	ssh_channel_free(channel);
	return SSH_OK;
}
