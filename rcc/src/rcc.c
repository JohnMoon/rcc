#include "rcc.h"
#include "net.h"
#include "parse_config.h"

void print_help() {
	fprintf(stdout, "Usage: rcc [options] file...\n");
	fprintf(stdout, "Options:\n");
	fprintf(stdout, "  -a <arch>\tTarget binary architecture <arch>\n");
	fprintf(stdout, "  -h <host>\trcc server hosted at <host>\n");
	fprintf(stdout, "  -p <port>\trcc server running on port <port>\n");
	fprintf(stdout, "  -u <user>\tSSH user running rcc server\n");
	fprintf(stdout, "  -l\t\tCompile the code locally\n");
	fprintf(stdout, "  -o <file>\tPlace the output into <file>\n");
	fprintf(stdout, "  -v\t\tVerbose output\n");
	fprintf(stdout, "  --help\tPrint this help\n\n");
	fprintf(stdout, "For more documentation, please see:\n");
	fprintf(stdout, "<https://gitlab.com/JohnMoon/rcc>\n");
}

int main(int argc, char **argv) {
	// Process config file and command line args
	assign_config_vars();
	process_args(argc, argv);

	if (local_compile) {
		compile_locally();
	}
	else {
		ssh_session session = establish_connection(host_server,
											       ssh_user, port_number);
		send_file(session, input_file);
		send_command(session, gen_compile_command(input_file));
		get_file(session, "/root/.rccd_build/a.out");
		disconnect(session);
	}
	return 0;
}

void assign_config_vars() {
	char *config_cross_arch = parse_config("MAIN", "ARCH");
	if (config_cross_arch != NULL)
		cross_arch = config_cross_arch;
	else
		fprintf(stderr, "warning - no ARCH entry in config... using default: %s\n", cross_arch);

	char *config_host_server = parse_config("NETWORK", "HOST");
	if (config_host_server != NULL)
		host_server = config_host_server;
	else
		fprintf(stderr, "warning - no HOST entry in config... using default: %s\n", host_server);

	char *config_port_number = parse_config("NETWORK", "PORT");
	if (config_port_number != NULL)
		port_number = atoi(config_port_number);
	else
		fprintf(stderr, "warning - no PORT entry in config... using default: %d\n", port_number);

	char *config_cflags = parse_config("OPTIONS", "CFLAGS");
	if (config_cflags != NULL)
		cflags = config_cflags;
}

void process_args(int argc, char **argv) {
	if (argc < 2) {
		print_help();
		exit(-1);
	}

	int c;
	opterr = 0;

	while ((c = getopt(argc, argv, "a:h:p:u:o:lv")) != -1) {
		switch (c)
		{
		case 'a':
			cross_arch = optarg;
			break;
		case 'h':
			host_server = optarg;
			break;
		case 'p':
			port_number = atoi(optarg);
			break;
		case 'u':
			ssh_user = optarg;
			break;
		case 'o':
			output_file = optarg;
			break;
		case 'l':
			local_compile = true;
			break;
		case 'v':
			verbose_output = true;
			break;
		case '?':
			if (optopt == 'c')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
		default:
			print_help();
			exit(-1);
		}
	}

	for (int i = optind; i < argc; i++) {
		if (access(argv[i], F_OK) != -1) {
			input_file = argv[i];
		}
		else {
			fprintf(stderr, "error - no such file or directory %s\n", argv[i]);
			exit(-1);
		}
	}
}

void compile_locally() {
	char command[256];
	snprintf(command, sizeof(command), "gcc %s -o %s %s", cflags, output_file, input_file);
	system(command);
}

char *gen_compile_command(char *filename) {
	char *command = malloc(sizeof(char) * 256);
   	snprintf(command, sizeof(command), "/usr/local/bin/rccd %s", filename);
	return command;
}
