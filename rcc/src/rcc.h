#include "common.h"
#include "net.h"
#include <ctype.h>

/* Global variables - options from config and command line */
bool local_compile = false;
bool verbose_output = false;
char *cross_arch = "x86";
char *host_server = "localhost";
uint16_t port_number = 22;
char *ssh_user = "root";
char *input_file;
char *output_file = "a.out";
char *cflags = "";

/* Assigns configuartion variables from config file */
void assign_config_vars();

/* Processes command line arguments */
void process_args(int argc, char **argv);

/* Compiles code locally */
void compile_locally();

/* Creates the compile command to send to the server */
char *gen_compile_command();
