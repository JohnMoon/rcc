#include <stdio.h>
#include <glib.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>

/* Parse the configuration file for the desired info */
char *parse_config(char *desiredSection, char *desiredKey);
