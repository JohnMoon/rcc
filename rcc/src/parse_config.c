#include "parse_config.h"

char *parse_config(char *desiredSection, char *desiredKey) {
	// TODO configurable config file path
	char *homedir = malloc(strlen(getenv("HOME")));
	strcpy(homedir, getenv("HOME"));
	char *configPath = malloc(strlen(homedir) + sizeof(char) * 15);
	strcpy(configPath, homedir);
	strcat(configPath, "/.rcc/rcc.ini");

	GKeyFile *gkf = g_key_file_new();

	if (!g_key_file_load_from_file(gkf, configPath, G_KEY_FILE_NONE, NULL)) {
        fprintf (stderr, "Could not read config file %s\n", configPath);
		return NULL;
    }

	free(homedir);
	free(configPath);

	return g_key_file_get_string(gkf, desiredSection, desiredKey, NULL);
}
