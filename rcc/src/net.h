#include <libssh/libssh.h>
#include <sys/stat.h>
#include <termios.h>
#include "common.h"

/* Establishes SSH session with host */
ssh_session establish_connection(char *host, char *ssh_user, uint16_t port);

/* Disconnects the current session */
void disconnect();

/* Sends command to SSH host */
int send_command(ssh_session session, char *command);

/* Sends file to SSH host */
int send_file(ssh_session session, char *file);

/* Gets file from SSH host */
int get_file(ssh_session session, char *file);

/* Gets password from user */
char *getPass(char *prompt);
