###############################################################################
#
# File: rcc-config-gen.sh
#
# Purpose: Generates a simple configuration file for RCC in the user's home
#          directory.
#
###############################################################################

#!/bin/sh

mkdir -p "${HOME}/.rcc/"
conf="${HOME}/.rcc/rcc.ini"

if [ "$#" == 1 ]; then
	conf="$1"
fi

printf "[MAIN]\n" > "$conf"
printf "ARCH=$(uname -m)\n" >> "$conf"

printf "[NETWORK]\n" >> "$conf"
printf "HOST=localhost\n" >> "$conf"
printf "PORT=22\n" >> "$conf"
